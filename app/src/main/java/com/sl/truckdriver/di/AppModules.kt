package com.sl.truckdriver.di

import com.google.gson.Gson
import com.sl.truckdriver.api.ApiErrorHandler
import com.sl.truckdriver.api.ApiManager
import com.sl.truckdriver.usecase.GetTripsUsecase
import com.sl.truckdriver.util.AnimationProvider
import com.sl.truckdriver.viewmodel.TripsMainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import org.koin.androidx.viewmodel.dsl.viewModel
val viewModelModule = module {
  viewModel { TripsMainViewModel(get()) }
}
val restModule = module {
    single { ApiManager( ) }
}

val errorHandlerModule = module {
    single {
        ApiErrorHandler(get())
    }
}

val jsonparserModule = module{
    single{ Gson() }
}

val useacseModule = module {
    factory { GetTripsUsecase(get(),get() )}

}
val animator = module{
    factory { AnimationProvider() }
}

val appModules = listOf(viewModelModule, restModule, errorHandlerModule,
    jsonparserModule,
    useacseModule, animator)