package com.sl.truckdriver.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sl.truckdriver.databinding.CellLocationsBinding
import com.sl.truckdriver.model.Stop
import com.sl.truckdriver.util.ResourceIdProvider

class LocationsAdapter(
    private val context: Context,
    private val items: ArrayList<Stop>,
    private val mLocationListener: LocationInteractionListener
) : RecyclerView.Adapter<LocationsAdapter.LocationViewHolder>() {
    inner class LocationViewHolder(val binding: CellLocationsBinding) :
        RecyclerView.ViewHolder(binding.root)

    interface LocationInteractionListener {
        fun onLocationClickListener(location: Stop)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocationViewHolder {
        val binding =
            CellLocationsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return LocationViewHolder(binding)
    }

    override fun onBindViewHolder(holder: LocationViewHolder, position: Int) {
        val item = items[position]
        holder.binding.tvLocationName.text = item.name
        holder.binding.rlLocationItem.setOnClickListener {
            mLocationListener.onLocationClickListener(item)
        }
        holder.binding.ivAction.setImageResource(ResourceIdProvider.getActionDrawableByAction(item.action.toString()))
    }

    override fun getItemCount(): Int {
        return items.size
    }

}