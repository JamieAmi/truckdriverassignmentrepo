package com.sl.truckdriver.adapter

import android.content.Context
import android.database.DatabaseUtils
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sl.truckdriver.R
import com.sl.truckdriver.databinding.CellTripBinding
import com.sl.truckdriver.model.Trip
import com.sl.truckdriver.util.DateUtils

class TripAdapter(private val context: Context, private val items:ArrayList<Trip>,private val mTripListener:TripInteractionListener) : RecyclerView.Adapter<TripAdapter.TripViewHolder>() {
    inner class TripViewHolder(val binding: CellTripBinding): RecyclerView.ViewHolder(binding.root) {}
    interface TripInteractionListener{
        fun onTripItemClickListener(trip:Trip)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TripViewHolder {
        val binding = CellTripBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TripViewHolder(binding)
    }
    override fun onBindViewHolder(holder: TripViewHolder, position: Int) {
        val item = items.get(position)

        holder.binding.tvStartDate.text = context.getText(R.string.starts_at_dataandtime).toString().replace("{date}",DateUtils.getLocalDateByUtcDateTime(
            item.startsAt.toString()
        ))
        holder.binding.tvStartTime.text = context.getText(R.string.starting_time).toString().replace("{time}",DateUtils.getLocalTimeByUtcDateTime(
            item.startsAt.toString()
        ))
        holder.binding.tvTripName.text = item.name.toString()
        holder.binding.rlItemWrapper.setOnClickListener {
            mTripListener.onTripItemClickListener(item)
        }
    }
    override fun getItemCount(): Int {
        return items.size
    }
}