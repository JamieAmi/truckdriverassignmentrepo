package com.sl.truckdriver.util

import com.sl.truckdriver.R

object ResourceIdProvider {
    fun getActionDrawableByAction(action:String):Int{
        return if(action == Constants.ACTION_LOAD){
            R.drawable.ic_load;
        }else{
            R.drawable.ic_unload;
        }
    }
}