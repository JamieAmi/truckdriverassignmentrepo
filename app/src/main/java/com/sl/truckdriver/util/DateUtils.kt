package com.sl.truckdriver.util

import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

object DateUtils {
    private var dateTimeFormat1 = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSXXX")
    private var dateFormat1 = SimpleDateFormat("yyyy MMMM dd")
    private var timeFormat1 = SimpleDateFormat("HH:mm")
    @JvmStatic
    fun getLocalDateByUtcDateTime(dateTime: String) : String {
        dateTimeFormat1.timeZone = TimeZone.getTimeZone("UTC")
        val date: Date = dateTimeFormat1 .parse(dateTime)
        dateFormat1.timeZone = TimeZone.getDefault()
        return dateFormat1.format(date).toString()
    }
    @JvmStatic
    fun getLocalTimeByUtcDateTime(dateTime: String) : String {
        dateTimeFormat1.timeZone = TimeZone.getTimeZone("UTC")
        val date: Date = dateTimeFormat1 .parse(dateTime)
        timeFormat1.timeZone = TimeZone.getDefault()
        return timeFormat1.format(date).toString()
    }
}