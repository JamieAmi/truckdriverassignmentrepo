package com.sl.truckdriver.util

import android.content.Context
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar

object SnackMessageCreator {
    fun createSnackBar(msg: String, snackRoot: View, context: Context, color: Int) {
        val snackError = Snackbar.make(snackRoot, msg, Snackbar.LENGTH_SHORT)
        val group = snackError.view as ViewGroup
        val tv = group.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
        tv.setTextColor(Color.WHITE)
        group.setBackgroundColor(ContextCompat.getColor(context, color))
        snackError.show()
    }
}