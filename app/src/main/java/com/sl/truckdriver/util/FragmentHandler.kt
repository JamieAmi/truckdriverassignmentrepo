package com.sl.truckdriver.util

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.sl.truckdriver.R

object FragmentHandler {
    fun replaceFragment(fragment: Fragment,containerId:Int,activity: AppCompatActivity) {
        val backstackName = fragment.javaClass.name
        val fragmentManager = activity.supportFragmentManager
        val fragmentPoped = fragmentManager.popBackStackImmediate(backstackName, 0)
        if (!fragmentPoped && fragmentManager.findFragmentByTag(backstackName) == null) {
            var fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(containerId, fragment)
            fragmentTransaction.addToBackStack(backstackName)
            fragmentTransaction.commit()
        }
    }

}