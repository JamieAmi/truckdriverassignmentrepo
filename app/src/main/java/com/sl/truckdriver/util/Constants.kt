package com.sl.truckdriver.util

object Constants {
    const val ACTION_LOAD = "Load"
    const val ACTION_UNLOAD = "Unload"
}