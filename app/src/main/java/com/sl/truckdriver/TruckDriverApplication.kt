package com.sl.truckdriver

import android.app.Application
import android.content.Context
import com.sl.truckdriver.api.VolleySingleton
import com.sl.truckdriver.di.appModules
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class TruckDriverApplication : Application() {


    override fun onCreate() {
        super.onCreate()
        VolleySingleton.init(this)
        startKoin {
          //  androidLogger()
            androidContext(applicationContext)
            modules(appModules)
        }

    }

    init {
        instance = this
    }

    companion object {
        private var instance: TruckDriverApplication? = null

        fun getApp(): Context {
            return instance!!.applicationContext
        }
    }
}