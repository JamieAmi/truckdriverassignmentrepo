package com.sl.truckdriver.usecase

import com.android.volley.Response
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.sl.truckdriver.api.ApiManager
import com.sl.truckdriver.model.Result
import com.sl.truckdriver.model.Trip
import java.lang.reflect.Type
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class GetTripsUsecase(private val apiManager: ApiManager, private val gson: Gson):BaseUsecase<GetTripsUsecase.RequestValues>() {
    class RequestValues():BaseUsecase.RequestValues

    override suspend fun executeUsecase(requestValues: RequestValues?) = suspendCoroutine<Result> { cont->
        apiManager.getTrips(Response.Listener {
            val tripListType: Type = object : TypeToken<ArrayList<Trip?>?>() {}.type
            val tripsList: ArrayList<Trip> = gson.fromJson(it.toString(),tripListType)
            cont.resume(Result.Success(tripsList))
        }, Response.ErrorListener { error ->
            cont.resume(Result.Failure(error))
        })
    }
}