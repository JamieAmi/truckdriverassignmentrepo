package com.sl.truckdriver.usecase

import com.sl.truckdriver.model.Result

abstract class BaseUsecase<Q:BaseUsecase.RequestValues> {
    var requestValues:Q? = null
    interface RequestValues
    abstract suspend fun executeUsecase(requestValues:Q?):Result

}