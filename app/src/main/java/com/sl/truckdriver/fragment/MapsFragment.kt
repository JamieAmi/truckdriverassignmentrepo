package com.sl.truckdriver.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.sl.truckdriver.R
import com.sl.truckdriver.viewmodel.TripsMainViewModel
import kotlinx.android.synthetic.main.content_toolbar.*
import org.koin.androidx.viewmodel.ext.android.getSharedViewModel

class MapsFragment : Fragment(),OnMapReadyCallback {
    private lateinit var tripViewModel: TripsMainViewModel
    private lateinit var mMap: GoogleMap
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_maps, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tripViewModel = getSharedViewModel<TripsMainViewModel>()
        tvTitle.text = getString(R.string.map)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }



    companion object {
        @JvmStatic
        fun newInstance():Fragment {
            return MapsFragment()
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        var location:String = tripViewModel.selectedStoreLatLng.value?.location.toString()
        val points: List<String> = location.split(",".toRegex())
        if(points != null && points.size ==2){
            val position = LatLng(points[0].toDouble(), points[1].toDouble())
            var mr:MarkerOptions = MarkerOptions ().position(position).title(tripViewModel.selectedStoreLatLng.value?.name)
            mMap.addMarker(mr)
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 12f))
        }
    }
}