package com.sl.truckdriver.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.sl.truckdriver.R
import com.sl.truckdriver.adapter.TripAdapter
import com.sl.truckdriver.model.Stop
import com.sl.truckdriver.model.Trip
import com.sl.truckdriver.util.AnimationProvider
import com.sl.truckdriver.viewmodel.TripsMainViewModel
import kotlinx.android.synthetic.main.content_common_list.*
import kotlinx.android.synthetic.main.content_toolbar.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.getSharedViewModel
import org.koin.core.parameter.parametersOf

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class TripsListFragment : Fragment(),TripAdapter.TripInteractionListener {
    private lateinit var tripAdapter: TripAdapter
    private lateinit var tripViewModel: TripsMainViewModel
    private  var trips:ArrayList<Trip> = ArrayList<Trip>()
    private  val animationProvider:AnimationProvider by inject<AnimationProvider>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_trips_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tripViewModel = getSharedViewModel<TripsMainViewModel>()
        setupUI()
        setupObservers()
        tripViewModel.getTrips()
    }

    private fun setupUI() {
        tvTitle.text = getString(R.string.trips)
        tripAdapter = TripAdapter(requireContext(), trips,this)
        val linearLayoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        rvGeneralList.layoutManager = linearLayoutManager
        rvGeneralList.adapter = tripAdapter
    }

    private fun setupObservers() {
        tripViewModel.trips.observe(requireActivity(), Observer { list ->
            trips.clear()
            trips.addAll(list)
            tripAdapter.notifyDataSetChanged()
            animationProvider.runLayoutAnimation(rvGeneralList)
        })
    }

    companion object {
        @JvmStatic
        fun newInstance():Fragment {
            return TripsListFragment()
        }
    }

    override fun onTripItemClickListener(trip: Trip) {
        tripViewModel.selectedTripLocations.postValue(trip.stops as ArrayList<Stop>?)
    }
}