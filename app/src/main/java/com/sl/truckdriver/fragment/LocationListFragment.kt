package com.sl.truckdriver.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.sl.truckdriver.R
import com.sl.truckdriver.adapter.LocationsAdapter
import com.sl.truckdriver.model.Stop
import com.sl.truckdriver.model.Trip
import com.sl.truckdriver.util.AnimationProvider
import com.sl.truckdriver.viewmodel.TripsMainViewModel
import kotlinx.android.synthetic.main.content_common_list.*
import kotlinx.android.synthetic.main.content_toolbar.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.getSharedViewModel
import org.koin.core.parameter.parametersOf


class LocationListFragment : Fragment(),LocationsAdapter.LocationInteractionListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var tripViewModel: TripsMainViewModel
    private  var locations:ArrayList<Stop> = ArrayList<Stop>()
    private lateinit var locationsAdapter:LocationsAdapter
    private  val animationProvider: AnimationProvider by inject<AnimationProvider>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_location_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tripViewModel = getSharedViewModel<TripsMainViewModel>()
        setupUI()
        setupObservers()
    }

    private fun setupObservers() {
        tripViewModel.selectedTripLocations.observe(requireActivity(), Observer {
            locations.clear()
            locations.addAll(it)
            locationsAdapter.notifyDataSetChanged()
            if(rvGeneralList != null) {
                animationProvider.runLayoutAnimation(rvGeneralList)
            }
        })
    }

    private fun setupUI() {
        tvTitle.text = getString(R.string.stops)
        locationsAdapter = LocationsAdapter(requireContext(),locations,this)
        val linearLayoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        rvGeneralList.layoutManager = linearLayoutManager
        rvGeneralList.adapter = locationsAdapter

    }

    companion object {
        @JvmStatic
        fun newInstance():Fragment {
            return LocationListFragment()
        }
    }

    override fun onLocationClickListener(stop: Stop) {
        tripViewModel.selectedStoreLatLng.postValue(stop)
    }
}