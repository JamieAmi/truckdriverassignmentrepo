package com.sl.truckdriver.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Stop {
    @SerializedName("location")
    @Expose
    var location: String? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("action")
    @Expose
    var action: String? = null
}