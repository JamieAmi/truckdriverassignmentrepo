package com.sl.truckdriver.model

import com.android.volley.VolleyError

sealed class Result {
    class Success(val response: List<Trip>?) : Result()
    class Failure(val error: VolleyError) : Result()

}