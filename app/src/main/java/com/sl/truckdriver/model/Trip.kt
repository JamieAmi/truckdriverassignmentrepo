package com.sl.truckdriver.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Trip{
    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("startsAt")
    @Expose
    var startsAt: String? = null

    @SerializedName("stops")
    @Expose
    var stops: List<Stop>? = null
}