package com.sl.truckdriver.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import com.sl.truckdriver.R
import com.sl.truckdriver.adapter.TripAdapter
import com.sl.truckdriver.api.ApiErrorHandler
import com.sl.truckdriver.fragment.LocationListFragment
import com.sl.truckdriver.fragment.MapsFragment
import com.sl.truckdriver.fragment.TripsListFragment
import com.sl.truckdriver.model.Trip
import com.sl.truckdriver.util.FragmentHandler
import com.sl.truckdriver.viewmodel.TripsMainViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.getViewModel
import org.koin.core.parameter.parametersOf

class MainActivity : AppCompatActivity() {
    private lateinit var  tripViewModel:TripsMainViewModel
    private val apiErrorHandler by inject<ApiErrorHandler> { parametersOf(this) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tripViewModel = getViewModel<TripsMainViewModel>()
        FragmentHandler.replaceFragment(TripsListFragment.newInstance(),R.id.flHomeContainer,this)
        setupListeners()
    }

    private fun setupListeners() {
        tripViewModel.apierror.observe(this, Observer {
            error ->
            apiErrorHandler.handleError(error,clMainContainer)
        })
        tripViewModel.selectedTripLocations.observe(this, Observer {
            FragmentHandler.replaceFragment(LocationListFragment.newInstance(),R.id.flHomeContainer,this)
        })
        tripViewModel.selectedStoreLatLng.observe(this, Observer {
            FragmentHandler.replaceFragment(MapsFragment.newInstance(),R.id.flHomeContainer,this)
        })
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            finish()
        } else {
            super.onBackPressed()
        }

    }

}