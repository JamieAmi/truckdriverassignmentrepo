package com.sl.truckdriver.viewmodel

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.volley.VolleyError
import com.sl.truckdriver.R
import com.sl.truckdriver.model.Result
import com.sl.truckdriver.model.Stop
import com.sl.truckdriver.model.Trip
import com.sl.truckdriver.usecase.GetTripsUsecase
import kotlinx.coroutines.launch

class TripsMainViewModel(private val tripsUsecase: GetTripsUsecase):ViewModel() {
    val apierror:MutableLiveData<VolleyError> = MutableLiveData()
    val trips:MutableLiveData<ArrayList<Trip>> = MutableLiveData()
    val selectedTripLocations:MutableLiveData<ArrayList<Stop>> = MutableLiveData()
    val selectedStoreLatLng:MutableLiveData<Stop> = MutableLiveData()
    fun getTrips(){
        viewModelScope.launch {
        val result:Result =    tripsUsecase.executeUsecase(GetTripsUsecase.RequestValues())
            when(result){
                is Result.Success->{
                    trips.postValue(result.response as ArrayList<Trip>?)
                }
                is Result.Failure ->{
                    apierror.postValue(result.error)
                }
            }
        }
    }

}