package com.sl.truckdriver.api

object UrlConstants {
    val BASE = "https://api.jsonbin.io/"
    val TRIPS = BASE + "b/616584d4aa02be1d44585283/latest"
}