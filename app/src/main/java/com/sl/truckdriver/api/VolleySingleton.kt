package com.sl.truckdriver.api

import android.content.Context
import com.android.volley.RequestQueue
import com.android.volley.toolbox.ImageLoader
import com.android.volley.toolbox.Volley

class VolleySingleton{

    companion object {
        private var mRequestQueue: RequestQueue? = null
        private lateinit var mImageLoader: ImageLoader
        private val MAX_IMAGE_CACHE_ENTIRES  = 100

        fun init(context: Context){
            mRequestQueue = Volley.newRequestQueue(context)
            mImageLoader = ImageLoader(mRequestQueue, BitmapLruCache(MAX_IMAGE_CACHE_ENTIRES))
        }

        fun getRequestQueue(): RequestQueue {
            if (mRequestQueue != null) {
                return mRequestQueue as RequestQueue
            }else{
                throw IllegalStateException("RequestQueue not initialized")
            }
        }

        fun getImageLoader(): ImageLoader {
            if(mImageLoader != null){
                return mImageLoader
            }else {
                throw IllegalStateException("ImageLoader not initialized")
            }
        }
    }


    fun init(context: Context) {
        mRequestQueue = Volley.newRequestQueue(context)
        mImageLoader = ImageLoader(mRequestQueue, BitmapLruCache(MAX_IMAGE_CACHE_ENTIRES))
    }
}