package com.sl.truckdriver.api

import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import org.json.JSONArray

class ApiManager {
    private fun getHeader(): MutableMap<String, String> {

        var headerValues: MutableMap<String, String> = HashMap<String, String>()
        headerValues.put("Accept", "application/json")
        headerValues.put("Content-Type", "application/json")
        headerValues.put("secret-key","\$2b\$10\$y2nMGsL6x9kz1HtcFKtBDeXEwm4QaS0vu8zVWaYBf5Qe/zfdMwJEO")

        return headerValues
    }
    fun getTrips(
        getTripsSuccessListener: Response.Listener<JSONArray>,
        getTripsErrorListener: Response.ErrorListener
    ) {

        val queue = VolleySingleton.getRequestQueue()
        val request = object : JsonArrayRequest(
            Request.Method.GET,
            UrlConstants.TRIPS,
            null,
            getTripsSuccessListener,
            getTripsErrorListener
        ) {
            override fun getHeaders(): MutableMap<String, String> {
                return getHeader()
            }
        }

        queue.add(request)
    }

}